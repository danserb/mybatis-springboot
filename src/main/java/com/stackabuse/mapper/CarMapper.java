package com.stackabuse.mapper;

import com.stackabuse.model.Car;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CarMapper {

    List<Car> selectCarsByBrand(String brand);

    void insertCar(Car car);

}
