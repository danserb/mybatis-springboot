package com.stackabuse;

import com.stackabuse.mapper.BookMapper;
import com.stackabuse.mapper.CarMapper;
import com.stackabuse.mapper.StudentMapper;
import com.stackabuse.model.Book;
import com.stackabuse.model.Car;
import com.stackabuse.model.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class MybatisTutorialApplication implements CommandLineRunner {

    private final static Logger LOGGER = LoggerFactory.getLogger(MybatisTutorialApplication.class);

    private CarMapper carMapper;
    private StudentMapper studentMapper;
    private BookMapper bookMapper;

    public MybatisTutorialApplication(CarMapper carMapper, StudentMapper studentMapper, BookMapper bookMapper) {
        this.carMapper = carMapper;
        this.studentMapper = studentMapper;
        this.bookMapper = bookMapper;
    }

    public static void main(String[] args) {
        SpringApplication.run(MybatisTutorialApplication.class, args);
    }

    @Override
    public void run(String... args) {
        List<Car> audiCars = carMapper.selectCarsByBrand("Audi");
        LOGGER.info("Found these Audi cars: {}", audiCars.toString());

        Car dacia = new Car("Dacia", "1300", "white");
        carMapper.insertCar(dacia);

        List<Car> daciaCars = carMapper.selectCarsByBrand("Dacia");
        LOGGER.info("Found these Dacia cars: {}", daciaCars.toString());

        List<Student> maleStudents = studentMapper.selectStudentByGender("male");
        LOGGER.info("These all are the male students: {}", maleStudents.toString());

        Book foundation = bookMapper.selectBookById(1L);
        LOGGER.info("Book found is: {}", foundation.toString());

        Book book = new Book("I Robot");
        bookMapper.insertBook(book);

        Book iRobot = bookMapper.selectBookById(2L);
        LOGGER.info("Book found is: {}", iRobot.toString());
    }
}
