package com.stackabuse.mapper;

import com.stackabuse.model.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentMapper {

    List<Student> selectStudentByGender(String gender);

}
