package com.stackabuse.model;

public class Car {

    private Long id;
    private String brand;
    private String model;
    private String color;

    public Car(String brand, String model, String color) {
        this.brand = brand;
        this.model = model;
        this.color = color;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brandName) {
        this.brand = brandName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String modelName) {
        this.model = modelName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brandName='" + brand + '\'' +
                ", modelName='" + model + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
