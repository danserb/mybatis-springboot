--schema.sql

drop table if exists car;
drop table if exists student;
drop table if exists book;

create table car (id int primary key auto_increment, brand varchar, model varchar, color varchar);

create table student (id int primary key auto_increment, student_name varchar, studentage int, gender varchar);

create table book (id int primary key auto_increment, name varchar);
