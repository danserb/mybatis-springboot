package com.stackabuse.mapper;

import com.stackabuse.model.Book;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface BookMapper {

    @Select("select id, name from book where id = #{id}")
    Book selectBookById(@Param("id") Long id);

    @Insert("insert into book (name) values #{name}")
    void insertBook(Book book);

}
