--data.sql

insert into car (brand, model, color) values ('Audi', 'RS6', 'Red');
insert into car (brand, model, color) values ('BMW', 'M5', 'Black');

insert into student(student_name, studentage, gender) values ('John', 20, 'male');
insert into student(student_name, studentage, gender) values ('Alisa', 22, 'female');

insert into book(name) values ('Foundation');
